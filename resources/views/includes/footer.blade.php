<div class="container">
    <section id="footer-bar">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <h4>Navigation</h4>
                <ul class="nav">
                    <li><a href="#">Homepage</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contac Us</a></li>
                    <li><a href="#">Your Cart</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4">
                <h4>My Account</h4>
                <ul class="nav">
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">Order History</a></li>
                    <li><a href="#">Wish List</a></li>
                    <li><a href="#">Newsletter</a></li>
                </ul>
            </div>
            <!-- Optional: clear the XS cols if their content doesn't match in height -->
            <div class="clearfix visible-xs-block"></div>
            <div class="col-xs-6 col-sm-4">
                <p class="logo"><img src="themes/images/logo.png"
                                     class="site_logo" alt=""></p>

                <br/>
                <span class="social_icons">
							<a class="facebook" href="#">Facebook</a>
							<a class="twitter" href="#">Twitter</a>
							<a class="skype" href="#">Skype</a>
							<a class="vimeo" href="#">Vimeo</a>
						</span>
            </div>
        </div>

    </section>
    <section id="copyright" style="text-align:center;">
        <span>Copyright 2017.  All right reserved.</span>
    </section>
</div>
</div>