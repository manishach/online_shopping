<div id="top-bar" class="container">
			<div class="row">
				<div class="span8" style="margin-left: 372px;">
					<div class="account pull-right">
						<ul class="user-menu">
							<li>
								<a href="{{ route('product.shoppingCart')}}">
									<i class="fa fa-shopping-cart"></i> Cart
									<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
								</a>
							</li>
							<li><a href="{{url('/checkout') }}">Checkout</a></li>
							<li class="dropdown">
							     @if (Auth::guest())
		                            <li><a href="{{ route('login') }}">Login</a></li>
		                            <li><a href="{{ route('register') }}">Register</a></li>
		                        @else
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
									<li>
										<a href="{{route('get.products')}}">
											Products
										</a>
									</li>

                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
							          
							</li>	
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="wrapper" class="container">
			<section class="navbar main-menu">
				<div class="navbar-inner main-menu">				
					<a href="{{url('/')}}" class="logo pull-left"><img src="themes/images/logo.png" class="site_logo" alt=""></a>
					<nav id="menu" class="pull-right">
						<ul>
							<li><a href="#">Woman</a>					
								<ul>
									<li><a href="#">Tops</a></li>
									<li><a href="#">Shirts</a></li>
									<li><a href="#">Kurthis</a></li>
								</ul>
							</li>															
							<li><a href="#">Man</a></li>			
							<li><a href="#">Sport</a>
								<ul>									
									<li><a href="#">Gifts and Tech</a></li>
									<li><a href="#">Ties and Hats</a></li>
									<li><a href="#">Cold Weather</a></li>
								</ul>
							</li>							
							<li><a href="#">Handbag</a></li>
							<li><a href="#">Best Seller</a></li>
							<li><a href="#">Top Seller</a></li>
						</ul>
					</nav>
				</div>
			</section>		
			</div>	