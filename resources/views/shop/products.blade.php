@extends('master')
@section('content')
  <div class="container">
      <div class="row" style="margin-bottom:20px;">
        <a href="{{route('add.product')}}" class="btn btn-success ">Add New Product</a>
      </div>
    @foreach($products->chunk(3) as $productChunk)
      <div class="row">
      @foreach($productChunk as $product)
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img src="images/shop/{{$product->imagePath}}" alt="..."  class="img-responsive">
              <!--<img src="{{$product->imagePath}}" alt="..."  class="img-responsive">-->
              <div class="caption">
                <h3>{{$product->title}}</h3>
                <p class="description">{{$product->description}}</p>
                <div class="clearfix"> 
                  <div class="pull-left price">${{$product->price}}</div>

                    <a href="{{route('product.addToCart',['id'=>$product->id])}}" class="btn btn-primary pull-right" role="button">Add to Cart</a>
                </div>
              </div>
            </div>
          </div>  
          @endforeach   
      </div>
    @endforeach    
  </div>
@endsection
