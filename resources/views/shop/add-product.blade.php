@extends('master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <form action="{{route('add.product')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
	            
				  <div class="form-group">
				    <label >Title</label>
				    <input type="text" class="form-control" name="title"  placeholder="Title">
				  </div>
				  <div class="form-group">
				    <label >Description</label>
				    <input type="text" class="form-control" name="description"  placeholder="Description">
				  </div>
				  <div class="form-group">
				    <label>price</label>
				    <input type="text" class="form-control"  name="price" placeholder="Price">
				  </div>
				  <div class="form-group">
			    	<label for="image">Upload Image</label>
   					 <input type="file"  name="image">
				  </div>
				  <button type="submit" class="btn btn-primary">Add Product</button>
				</form>
            </div>
          </div>  
	</div>
</div>
@endsection
