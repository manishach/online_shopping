@extends('master')

@section('content')
    @include('includes.slider')
    <div class="container">
        <div class="row">
            <div class="span11">
                <h4 class="title">
                    <span class="pull-left"><span class="text"><span
                                    class="line">Feature <strong>Products</strong></span></span></span>
                    <span class="pull-right">
										<a class="left button" href="#myCarousel" data-slide="prev"></a><a
                                class="right button" href="#myCarousel" data-slide="next"></a>
									</span>
                </h4>
            </div>
        </div>

        @foreach($products->chunk(4) as $productChunk)
            <div class="row">
                @foreach($productChunk as $product)
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <img src="images/shop/{{$product->imagePath}}" alt="..." class="img-responsive">
                        <!--<img src="{{$product->imagePath}}" alt="..."  class="img-responsive">-->
                            <div class="caption">
                                <h3>{{$product->title}}</h3>
                                <p class="description">{{$product->description}}</p>
                                <div class="clearfix">
                                    <div class="pull-left price">${{$product->price}}</div>
                                    <a href="{{route('product.addToCart',['id'=>$product->id])}}"
                                       class="btn btn-primary pull-right" role="button">Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endsection