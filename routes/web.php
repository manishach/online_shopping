<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('shop.index');
});
*/
Route::get('/',[
    'as'=>'shop.index',
    'uses'=>'HomeController@getShopIndex'
]);

Route::get('/products',[
	'as'=>'get.products',
	'uses'=>'ProductController@getProducts'
	]);
Route::get('/add-product',[
	'as'=>'add.product',
	'uses'=>'ProductController@addProduct'
	]);
Route::post('/add-product',[
	'as'=>'add.product',
	'uses'=>'ProductController@postProduct'
	]);

Route::get('/add-to-cart/{id}',[
    'as'=>'product.addToCart',
    'uses'=>'ProductController@getAddToCart'
]);
Route::get('/shopping-cart',[
    'as'=>'product.shoppingCart',
    'uses'=>'ProductController@getCart'
]);
Route::get('/checkout',[
    'as'=>'checkout',
    'uses'=>'ProductController@getCheckout'

]);
Route::post('/checkout',[
    'as'=>'checkout',
    'uses'=>'ProductController@postCheckout'
]);
Auth::routes();

Route::get('/home', [
   'uses'=> 'HomeController@index',
    'as'=> 'product.index'
]);
