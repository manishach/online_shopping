<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $product = new \App\Product([
        	'imagePath' =>'images/shop/blue-dress.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 15
        	]);
        $product->save();
        $product = new \App\Product([
        	'imagePath' =>'images/shop/product13.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 25
        	]);
        $product->save();
        $product = new \App\Product([
        	'imagePath' =>'images/shop/product12.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 12
        	]);
        $product->save();
        $product = new \App\Product([
        	'imagePath' =>'images/shop/product11.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 15
        	]);
        $product->save();
        $product = new \App\Product([
        	'imagePath' =>'images/shop/product10.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 25
        	]);
        $product->save();
        $product = new \App\Product([
        	'imagePath' =>'images/shop/blue-dress.jpg',
        	'title' =>'Dresses',
        	'description' =>'Lasted Designed Dresses',
        	'price' => 12
        	]);
        $product->save();
    }
}
