<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use App\Cart;

class ProductController extends Controller
{
	public function getProducts()
    {
    	$products =Product::all();     
    	return view('shop.products',['products'=>$products]);

    }
   public function addProduct()
    {
      return view('shop.add-product');
    }
    public function postProduct(Request $request)
    {
    	$this->validate($request,[
          'title'=>'required',
          'description'=>'required',
          'price'=>'required',
          'imagePath'=>'image|mimes:png,jpg,jpeg|max:10000',
          ]);

          $image = $request->file('image');
          $destinationPath = 'images/shop/';
          $filename = str_random(6).'_'.$image->getClientOriginalName();
          $filename = $image->getClientOriginalName();
          $uploaded = $image->move($destinationPath, $filename);

          $product = new Product;
          $product->title = $request->title;
          $product->description = $request->description;
          $product->price = $request->price;
          $product ->imagepath = $filename;
          $product->save();
          return redirect(route('get.products'));
    }
    public function getAddToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product,$product->id);
        $request->session()->put('cart', $cart);
        return redirect(route('product.index'));
    }
    public function getCart()
    {
        if(!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view ('shop.shopping-cart',['products'=>$cart->items,'totalPrice'=>$cart->totalPrice]);
    }
    public function getCheckout()
    {
        if(!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart( $oldCart);
        $total = $cart->totalPrice;
        return view('shop.checkout',['total'=>$total]);

    }
    public function postCheckout(){
    echo 'Checked-out Successfully';
       // return view('checkout');
    }
}
