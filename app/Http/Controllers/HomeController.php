<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products =Product::all();
       // return view('welcome',['products'=>$products]);
        return view('home',['products'=>$products]);
    }
    public function getShopIndex()
    {
        $products =Product::all();
        return view('shop.index',['products'=>$products]);
    }
}
